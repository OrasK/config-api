<?php

use \Psr\Http\Message\ServerRequestInterface as Request;
use \Psr\Http\Message\ResponseInterface as Response;

require '../../vendor/autoload.php';

$app = new \Slim\App;
$app->get('/config/{domain}/{area}/{key}', function (Request $request, Response $response) {
    $domain = $request->getAttribute('domain');
    $area = $request->getAttribute('area');
    $key = $request->getAttribute('key');

    $configArray = [
        $domain => [
            $area => [
                $key => 10
            ]
        ],
    ];
    $response = $response->withHeader('Content-Type', 'application/json');
    $response->withStatus(200);
    $response->getBody()->write(json_encode($configArray));

    return $response;
});
$app->run();